/***************************************************
 ** @Desc : This file for 转账js
 ** @Time : 2019.04.13 14:14
 ** @Author : Joker
 ** @File : transfer
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.13 14:14
 ** @Software: GoLand
 ****************************************************/

let transfer = {
    transfer_do_paging: function () {
        let accountName = $("#accountName").val();
        let datetimepicker1 = $("#datetimepicker1").val();
        let datetimepicker2 = $("#datetimepicker2").val();
        let uStatus = $("#uStatus").val();
        let _userName = $("#_t_userName").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/transfer/list_transfer/",
            data: {
                page: '1',
                limit: "20",
                MerchantName: accountName,
                start: datetimepicker1,
                end: datetimepicker2,
                uStatus: uStatus,
                _userName: _userName,
            }, //参数：当前页为1
            success: function (data) {
                transfer.transfer_pay_data(data.root, data.rechargeT);//处理数据

                let options = {//根据后台返回的分页相关信息，设置插件参数
                    bootstrapMajorVersion: 3, //
                    currentPage: data.page, //当前页数
                    totalPages: data.totalPage, //总页数
                    numberOfPages: data.limit,//每页记录数
                    itemTexts: function (type, page) {//设置分页按钮显示字体样式
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                        $.ajax({//根据page去后台加载数据
                            url: "/transfer/list_transfer/",
                            type: "GET",
                            data: {
                                page: page,
                                _userName: _userName,
                                MerchantName: accountName,
                                start: datetimepicker1,
                                end: datetimepicker2,
                                uStatus: uStatus,
                            },
                            success: function (data) {
                                transfer.transfer_pay_data(data.root, data.rechargeT);//处理数据
                            }
                        });
                    }
                };
                $('#xf_page').bootstrapPaginator(options);//设置分页
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    transfer_pay_data: function (list,amount) {
        let con = "";
        $.each(list, function (index, item) {
            let st = "";
            switch (item.Status) {
                case "F":
                    st = "失败";
                    break;
                case "I":
                    st = "处理中";
                    break;
                case "S":
                    st = "成功";
                    break
            }

            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.TrOrderId + "</td>";
            con += "<td>" + item.TrAmount + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "<td>" + st + "</td>";
            con += "<td>" + item.Remark + "</td>";

            if (item.Status === "I") {
                con += "<td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"transfer.transfer_query(" + item.Id + ");\" title=\"查询\"><i class=\"glyphicon glyphicon-zoom-in\"></i>查询</button></td>";
            }

            con += "</tr>";
        });
        $("#your_showtime").html(con);
        $("#_t_recharge_amount").html(amount);
    },
    transfer_query: function (no) {
        $.ajax({
            type: "GET",
            url: "/transfer/transfer_query/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal("更新成功！", {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        transfer.transfer_do_paging();
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
};